import 'package:farm_tracking_app/main.dart';
import 'package:farm_tracking_app/mypages/features.dart';
import 'package:flutter/material.dart';

import 'edit_register.dart';

void main() {
  runApp(const MyApp());
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text('L O G I N'),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromARGB(255, 9, 29, 2),
            ),
            child: ListView(
              children: [
                const SizedBox(height: 20),
                Padding(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                    child: Container(
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/Farmlogo.png'))),
                      alignment: Alignment.center,
                      width: 300,
                      height: 300,
                    )),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: Container(
                    alignment: Alignment.center,
                    width: 200,
                    height: 100,
                    child: const Text(
                      'WELCOME',
                      style: TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.w500,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(300),
                          ),
                          child: const Center(
                            child: Icon(Icons.person),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(00.0),
                        child: Container(
                          width: 400,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: const Center(
                            child: TextField(
                              cursorColor: Colors.red,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Username',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 8, 20, 8),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(300),
                          ),
                          child: const Center(child: Icon(Icons.mail)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(00.0),
                        child: Container(
                          width: 400,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: const Center(
                            child: TextField(
                              cursorColor: Colors.red,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Email',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 8, 20, 8),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(300),
                          ),
                          child: const Center(
                            child: Icon(Icons.lock),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          width: 400,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: const Center(
                            child: TextField(
                              cursorColor: Colors.red,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Password',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 8, 20, 8),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(
                        width: 100,
                        height: 50,
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            width: 140,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: TextButton.icon(
                                  label: const Text(
                                    'SIGN IN',
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  icon: Icon(
                                    size: 30,
                                    Icons.login_rounded,
                                    color: Color.fromARGB(255, 0, 0, 0),
                                  ),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Dashboard()))),
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                            width: 140,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: TextButton.icon(
                                  label: const Text(
                                    'SIGN UP',
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  icon: Icon(
                                    size: 30,
                                    Icons.app_registration_sharp,
                                    color: Color.fromARGB(255, 5, 36, 61),
                                  ),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Register()))),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: TextButton.icon(
                      label: const Text(
                        'Forgot Password?',
                        style: TextStyle(
                            color: Color.fromARGB(255, 219, 16, 16),
                            fontStyle: FontStyle.italic,
                            fontSize: 17),
                      ),
                      icon: Icon(
                        size: 30,
                        Icons.login_rounded,
                        color: Colors.transparent,
                      ),
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  const MyApp()))),
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),

            // This trailing comma makes auto-formatting nicer for build methods.
          ),
        ));
  }
}

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text('D A S H B O A R D '),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: const Color.fromARGB(255, 9, 29, 2),
              ),
              child: ListView(
                children: [
                  const SizedBox(height: 50),
                  const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: SizedBox(
                      width: 200,
                      height: 50,
                      child: Icon(
                        Icons.dashboard,
                        size: 100,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(height: 50),
                  Column(children: [
                    Center(
                      child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                              height: 300,
                              width: 300,
                              decoration: BoxDecoration(
                                  color: const Color.fromARGB(255, 4, 168, 9),
                                  borderRadius: BorderRadius.circular(10)),
                              child: OutlinedButton.icon(
                                  icon: const Text(
                                    'PRODUCTS',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  label: Icon(
                                    Icons.production_quantity_limits_rounded,
                                    size: 50,
                                    color: Colors.black,
                                  ),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Products()))))),
                    ),
                    Center(
                      child: Padding(
                          //edit profil
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                              height: 300,
                              width: 300,
                              decoration: BoxDecoration(
                                  color: const Color.fromARGB(255, 3, 63, 63),
                                  borderRadius: BorderRadius.circular(10)),
                              child: OutlinedButton.icon(
                                  icon: const Text(
                                    'GALLERY',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color:
                                            Color.fromARGB(255, 255, 255, 255)),
                                  ),
                                  label: Icon(
                                    Icons.photo,
                                    size: 50,
                                    color: Colors.black,
                                  ),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Gallery()))))),
                    ),
                    Center(
                      child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                              height: 300,
                              width: 300,
                              decoration: BoxDecoration(
                                  color: const Color.fromARGB(255, 168, 143, 4),
                                  borderRadius: BorderRadius.circular(10)),
                              child: OutlinedButton.icon(
                                  icon: const Text(
                                    ' EDIT PROFILE',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  label: Icon(
                                    Icons.edit_outlined,
                                    size: 50,
                                    color: Colors.black,
                                  ),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const EditProfil()))))),
                    ),
                    Center(
                        child: Padding(
                            //edit profil
                            padding: const EdgeInsets.all(20.0),
                            child: Container(
                                height: 30,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: const Color.fromARGB(255, 8, 20, 88),
                                    borderRadius: BorderRadius.circular(10)),
                                child: OutlinedButton.icon(
                                    icon: const Text(
                                      'LOGOUT',
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    label: Icon(Icons.logout_outlined,
                                        size: 25,
                                        color: Color.fromARGB(255, 14, 4, 3)),
                                    onPressed: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                const LoginPage())))))),
                  ])
                ],
              )),
        ));
  }
}
