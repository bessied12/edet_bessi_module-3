import 'package:farm_tracking_app/main.dart';
import 'package:flutter/material.dart';

import 'login_dashboard.dart';

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 3, 19, 46),
          title: const Text(
            'R E G I S T E R',
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: const Color.fromARGB(255, 9, 29, 2),
          ),
          child: Builder(builder: (context) {
            return ListView(
              padding: const EdgeInsets.all(20.0),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 45,
                      child: Icon(
                        Icons.edit,
                        size: 80,
                        color: Color.fromARGB(255, 10, 199, 155),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    const Text(
                      'Welcome to Farm Tracker',
                      style: TextStyle(
                          fontSize: 40,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      'Please fill up the form below',
                      style: TextStyle(
                          fontSize: 40,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),

                    const SizedBox(
                      height: 70,
                    ),
                    const Text(
                      "FIRST NAME",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.white),
                    ),

                    //ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: const Color.fromARGB(255, 248, 245, 245),
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(6, 2),
                                blurRadius: 8.0,
                                spreadRadius: 5.0,
                              ),
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(-6, -2),
                                blurRadius: 6.0,
                                spreadRadius: 3.0,
                              ),
                            ]),
                        child: const Center(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '@Nathalie Rosman',
                              contentPadding:
                                  EdgeInsets.fromLTRB(20, 10, 12, 8),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),

                    const Text(
                      "USERNAME",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    ),

                    //ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.white,
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(6, 2),
                                blurRadius: 8.0,
                                spreadRadius: 5.0,
                              ),
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(-6, -2),
                                blurRadius: 6.0,
                                spreadRadius: 3.0,
                              ),
                            ]),
                        child: const Center(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '@Rosman_009 ',
                              contentPadding: EdgeInsets.fromLTRB(20, 8, 12, 8),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "PASSWORD",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    ),

                    // ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.white,
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(6, 2),
                                blurRadius: 8.0,
                                spreadRadius: 5.0,
                              ),
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(-6, -2),
                                blurRadius: 6.0,
                                spreadRadius: 3.0,
                              ),
                            ]),
                        child: const Center(
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: '',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 8, 12, 8)),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 40),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                                height: 40,
                                width: 150,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: Colors.black),
                                child: OutlinedButton.icon(
                                    icon: const Text('SEND',
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 236, 232, 232),
                                            fontSize: 14)),
                                    label: const Icon(
                                      Icons.send_rounded,
                                      color: Color.fromARGB(255, 8, 89, 121),
                                    ),
                                    onPressed: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                const LoginPage()))))),
                      ],
                    ),
                  ],
                ),
              ],
            );
          }),
        ));
  }
}

class EditProfil extends StatelessWidget {
  const EditProfil({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Row(
          children: const [
            Text(
              'E D I T  P R O F I L E',
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ],
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: const Color.fromARGB(255, 9, 29, 2),
          ),
          child: Builder(builder: (context) {
            return ListView(
              padding: const EdgeInsets.all(20.0),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 45,
                      child: Icon(Icons.edit_attributes_outlined,
                          size: 80, color: Colors.black),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    const Text(
                      'EDIT PROFIL',
                      style: TextStyle(
                          fontSize: 40,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),

                    const SizedBox(
                      height: 50,
                    ),
                    const Text(
                      "FIRST NAME",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.white),
                    ),

                    //ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: boxdecoration(),
                        child: const Center(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '@Nathalie Roseman ',
                              contentPadding: EdgeInsets.fromLTRB(20, 8, 12, 8),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),

                    const Text(
                      "USERNAME",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    ),

                    //ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: boxdecoration(),
                        child: const Center(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '@Roseman_0089 ',
                              contentPadding: EdgeInsets.fromLTRB(20, 8, 12, 8),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "PASSWORD",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    ),

                    // ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: boxdecoration(),
                        child: const Center(
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: '**********',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 8, 12, 8)),
                          ),
                        ),
                      ),
                    ),
                    const Text(
                      "CONFIRM PASSWORD",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    ),

                    // ignore: avoid_unnecessary_containers
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 400,
                        decoration: boxdecoration(),
                        child: const Center(
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: '**********',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 8, 12, 8)),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                                height: 40,
                                width: 150,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: Colors.black),
                                child: OutlinedButton.icon(
                                    icon: const Text('CONFIRM',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400)),
                                    label: const Icon(Icons.save,
                                        color: Colors.white),
                                    onPressed: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                const MyApp()))))),
                      ],
                    ),
                  ],
                ),
              ],
            );
          }),
        ),
      ),
    );
  }

  BoxDecoration boxdecoration() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            offset: Offset(6, 2),
            blurRadius: 8.0,
            spreadRadius: 5.0,
          ),
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            offset: Offset(-6, -2),
            blurRadius: 6.0,
            spreadRadius: 3.0,
          ),
        ]);
  }
}
